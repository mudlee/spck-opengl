package spck.game.nations;

public interface Nation {
    CityArea[] getAreas();
}
